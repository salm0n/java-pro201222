import javax.persistence.*;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;

public class DBManager{
    private EntityManagerFactory emf;
    private EntityManager em;
    Scanner sc = new Scanner(System.in);

    public void init(){
        emf = Persistence.createEntityManagerFactory("JPAex7");
        em = emf.createEntityManager();
    }

    public void close(){
        emf.close();
        em.close();
    }

    public void addToDB(){
        String dbName = showOptions();
        Object obj;

        switch (dbName){
            case "1":
                System.out.print("Enter client name: ");
                String sName = sc.nextLine();
                System.out.print("Enter client phone: ");
                String sPhone = sc.nextLine();
                System.out.print("Enter client mail: ");
                String sMail = sc.nextLine();

                obj = new Client(sName, sMail, sPhone);
                break;
            case "2":
                System.out.print("Enter product name: ");
                String productName = sc.nextLine();
                System.out.print("Enter product price: ");
                String productPrice = sc.nextLine();
                obj = new Product(productName, Double.parseDouble(productPrice));
                break;
            case "3": makeAnOrder();
            default: return;
        }

        performTransaction(() -> {
            em.persist(obj);
            return null;
        });
    }

    public void changeInDB(){
        String dbName = showOptions();

        if (dbName.equals("1") || dbName.equals("2") || dbName.equals("3")){
            System.out.print("Enter id: ");
            String answer = sc.nextLine();
            long id = Long.parseLong(answer);
            if (dbName.equals("1")) changeClient(id);
            if (dbName.equals("2")) changeProduct(id);
            if (dbName.equals("3")) changeOrder(id);
        }
    }

    public void makeAnOrder(){
        System.out.println("Enter client id: ");
        String clientId = sc.nextLine();
        System.out.println("Enter product id: ");
        String productId = sc.nextLine();

        Query queryClient = em.createQuery("select x from Client x where x.id = :id", Client.class);
        queryClient.setParameter("id", Long.parseLong(clientId));

        Query queryProduct = em.createQuery("select c from Product c where c.id = :id", Product.class);
        queryProduct.setParameter("id", Long.parseLong(productId));

        performTransaction(() -> {
            queryProduct.getSingleResult();
            Client cl = (Client) queryClient.getSingleResult();
            Product pr = (Product) queryProduct.getSingleResult();
            Order order = new Order(cl, pr);
            order.setOrderStatus("ACTIVE");
            em.persist(order);
            return null;
        });
    }


    private void changeClient(long id){
        Client cl;

        try{
            Query query = em.createQuery("select x from Client x where x.id = :id" , Client.class);
            query.setParameter("id", id);
            cl = (Client) query.getSingleResult();
        }catch (NoResultException ex){
            System.err.println("Client not found");
            return;
        }catch (NonUniqueResultException ex){
            System.err.println("Client not unique");
            return;
        }

        System.out.println("1 - name");
        System.out.println("2 - phone");
        System.out.println("3 - mail");
        String res = sc.nextLine();
        System.out.print("new value: ");
        String newValue = sc.nextLine();

        performTransaction(() -> {
            switch (res) {
                case "1" -> cl.setName(newValue);
                case "2" -> cl.setPhone(newValue);
                case "3" -> cl.setMail(newValue);
            }
            return null;
        });
    }

    private void changeProduct(long id){
        Product pr;

        try{
            Query query = em.createQuery("select x from Product x where x.id = :id" , Product.class);
            query.setParameter("id", id);
            pr = (Product) query.getSingleResult();
        }catch (NoResultException ex){
            System.err.println("Product not found");
            return;
        }catch (NonUniqueResultException ex){
            System.err.println("Product not unique");
            return;
        }

        System.out.println("1 - name");
        System.out.println("2 - price");
        String res = sc.nextLine();
        System.out.print("new value: ");
        String newValue = sc.nextLine();

        performTransaction(() -> {
            if (res.equals("1")) pr.setProductName(newValue);
            else if (res.equals("2")) pr.setPrice(Double.parseDouble(newValue));
            return null;
        });
    }

    private void changeOrder(Long id){
        Order order;

        try{
            Query query = em.createQuery("select x from Order x where x.id = :id" , Order.class);
            query.setParameter("id", id);
            order = (Order) query.getSingleResult();
        }catch (NoResultException ex){
            System.err.println("Order not found");
            return;
        }

        System.out.println("1 - make order active");
        System.out.println("2 - cancel the order");
        System.out.println("3 - complete the order");
        String res = sc.nextLine();

        performTransaction(() -> {
            if (res.equals("1")) order.setOrderStatus("ACTIVE");
            if (res.equals("2")) order.setOrderStatus("CANCELED");
            if (res.equals("3")) order.setOrderStatus("COMPLETED");
            return null;
        });
    }



    public void view (){
        String answer = showOptions();

        switch (answer) {
            case "1" -> view(Client.class, "Client");
            case "2" -> view(Product.class, "Product");
            case "3" -> view(Order.class, "Order");
        }
    }


    private <T>void view (T t, String name){
        Query query = em.createQuery("SELECT c FROM " + name +" c", (Class<T>)t);

        List<T> list = (List<T>) query.getResultList();
        for (T o : list)
            System.out.println(o);

    }

    private String showOptions(){
        System.out.println("1 - Client");
        System.out.println("2 - Product");
        System.out.println("3 - Order");
        return sc.nextLine();
    }


    private  <T> T performTransaction(Callable<T> action) {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        try {
            T result = action.call();
            transaction.commit();

            return result;
        } catch (Exception ex) {
            if (transaction.isActive()){
                transaction.rollback();
            }
            ex.printStackTrace();
            return null;
        }
    }
}