package main;

import java.lang.reflect.*;

public class Main {

    public static void main(String[] args) {

        Class<?> cls = TextContainer.class;
        if (cls.isAnnotationPresent(SaveTo.class)){
            Method[] methods = cls.getDeclaredMethods();
            SaveTo an = cls.getAnnotation(SaveTo.class);
            TextContainer txtCont = new TextContainer();
            for(Method method : methods){
                if (method.isAnnotationPresent(Saver.class)){
                    try{
                        method.invoke(txtCont, an.path(), txtCont.getStr());
                    }catch(IllegalAccessException | InvocationTargetException e){
                        e.printStackTrace();
                    }
                    break;
                }
            }

            System.out.println("Success");
        }
    }


}
