package com.example.hw1;

import jakarta.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class SampleServlet extends HttpServlet {

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String TEMPLATE = "<html><head><title>HomeWork1</title></head><body><h1>Hello, %s !!!</h1></body></html>";
        String name = req.getParameter("name");
        resp.setContentType("text/html");
        PrintWriter pw = resp.getWriter();
        pw.println(String.format(TEMPLATE, name));
    }
}
