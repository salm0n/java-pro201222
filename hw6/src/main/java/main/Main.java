package main;

import javax.persistence.*;
import java.util.List;
import java.util.Scanner;

public class Main {
    static EntityManagerFactory emf;
    static EntityManager em;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try{
            emf = Persistence.createEntityManagerFactory("JPAex6");
            em = emf.createEntityManager();
            boolean flag = true;
            try{
                while(flag){
                    System.out.println("1 - add Apartment");
                    System.out.println("2 - delete Apartment");
                    System.out.println("3 - change Apartment");
                    System.out.println("4 - view Apartments");
                    System.out.print("-> ");

                    String s = sc.nextLine();

                    switch (s){
                        case "1" -> addApartm(sc);
                        case "2" -> deleteApartm(sc);
                        case "3" -> changeApartm(sc);
                        case "4" -> viewApartm(sc);

                        default -> flag = false;
                    }
                }
            }finally {
                sc.close();
                em.close();
                emf.close();
            }

        }catch (Exception ex){
            ex.printStackTrace();
            return;
        }
    }

    private static void addApartm(Scanner sc){
        System.out.println("Enter apartment address");
        String address = sc.nextLine();
        System.out.println("Enter apartment area");
        String sArea = sc.nextLine();
        System.out.println("Enter apartment price");
        String sPrice = sc.nextLine();
        System.out.println("Enter roomCount");
        String sRoomCount = sc.nextLine();
        System.out.println("Enter apartment district");
        String district = sc.nextLine();

        double price = Double.parseDouble(sPrice);
        double area = Double.parseDouble(sArea);
        int roomCount = Integer.parseInt(sRoomCount);

        em.getTransaction().begin();

        try{
            Apartment app = new Apartment(address, district, area, roomCount, price);
            em.persist(app);
            em.getTransaction().commit();
            System.out.println(app.getId());
        }catch (Exception ex){
            em.getTransaction().rollback();
        }
    }

    private static void deleteApartm(Scanner sc){
        System.out.println("Enter apartment id");
        String sId = sc.nextLine();
        long id = Long.parseLong(sId);
        Apartment apartm = em.getReference(Apartment.class, id);

        if(apartm == null){
            System.out.println("apartment not found!");
            return;
        }

        em.getTransaction().begin();

        try{
            em.remove(apartm);
            em.getTransaction().commit();
        }catch (Exception ex){
            em.getTransaction().rollback();
        }
    }

    private static void changeApartm(Scanner sc){
        System.out.println("Enter apartment id");
        String sId = sc.nextLine();
        long id = Long.parseLong(sId);

        Apartment apartm = null;
        try{
            Query query = em.createQuery("select x from Apartment x where x.id = :id" , Apartment.class);
            query.setParameter("id", id);
            apartm = (Apartment) query.getSingleResult();
        }catch (NoResultException ex){
            System.err.println("Apartment not found");
            return;
        }catch (NonUniqueResultException ex){
            System.err.println("Apartment not unique");
            return;
        }

        System.out.println("What to change?");
        System.out.println("1 - address");
        System.out.println("2 - district");
        System.out.println("3 - area");
        System.out.println("4 - room count");
        System.out.println("5 - price");
        System.out.print("-> ");
        String changeParam = sc.nextLine();

        System.out.print("Enter new value: ");
        String value = sc.nextLine();

        em.getTransaction().begin();

        try{
            switch (changeParam){
                case "1" -> apartm.setAddress(value);
                case "2" -> apartm.setDistrict(value);
                case "3" -> apartm.setArea(Double.parseDouble(value));
                case "4" -> apartm.setRoomCount(Integer.parseInt(value));
                case "5" -> apartm.setPrice(Double.parseDouble(value));
            }

            em.getTransaction().commit();
        }catch (Exception ex){
            em.getTransaction().rollback();
        }
    }

    private static void viewApartm(Scanner sc){
        String reqValue = "";

        System.out.println("Select parameter: ");
        System.out.println("1 - address");
        System.out.println("2 - district");
        System.out.println("3 - area");
        System.out.println("4 - room count");
        System.out.println("5 - price");
        System.out.print("-> ");
        String answer = sc.nextLine();

        if (answer.equals("")){
            viewAllApatm();
            return;
        }

        System.out.println("value: ");
        String param = sc.nextLine();

        switch (answer){
            case "1" -> reqValue = "address";
            case "2" -> reqValue = "district";
            case "3" -> reqValue = "area";
            case "4" -> reqValue = "roomCount";
            case "5" -> reqValue = "price";

        }

        Query query = em.createQuery(String.format("SELECT c FROM Apartment c where c.%s = :value", reqValue), Apartment.class);
        
        if (reqValue.equals("area") || reqValue.equals("price")) query.setParameter("value", Double.parseDouble(param));

        else if (reqValue.equals("roomCount")) query.setParameter("value", Integer.parseInt(param));

        else query.setParameter("value", param);

        List<Apartment> list = (List<Apartment>) query.getResultList();

        for (Apartment c : list)
            System.out.println(c);

    }

    private static void viewAllApatm() {
        Query query = em.createQuery("SELECT c FROM Apartment c", Apartment.class);
        List<Apartment> list = (List<Apartment>) query.getResultList();

        for (Apartment c : list)
            System.out.println(c);
    }
}