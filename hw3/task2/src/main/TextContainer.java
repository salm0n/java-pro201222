package main;

import java.io.IOException;
import java.io.PrintWriter;

@SaveTo(path = "sample2.txt")
public class TextContainer {
    private String str = "testing reflection";

    public TextContainer(){}

    public TextContainer(String str) {
        this.str = str;
    }

    public String getStr (){
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return "TextContainer{" +
                "str='" + str + '\'' +
                '}';
    }

    @Saver
    public void save (String path, String txt){
        try(PrintWriter pw = new PrintWriter(path)){
            pw.println(txt);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
