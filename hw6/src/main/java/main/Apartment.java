package main;

import javax.persistence.*;

@Entity
@Table(name = "Apartments")
public class Apartment {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private String district;

    @Column(nullable = false)
    private double area;

    @Column(nullable = false)
    private int roomCount;

    @Column(nullable = false)
    private double price;


    public Apartment(){}

    public Apartment(String address, String district, double area, int roomCount, double price) {
        this.address = address;
        this.district = district;
        this.area = area;
        this.roomCount = roomCount;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getDistrict() {
        return district;
    }

    public double getArea() {
        return area;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public double getPrice() {
        return price;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    @Override
    public String toString() {
        return "Apartment{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", district='" + district + '\'' +
                ", area=" + area +
                ", roomCount=" + roomCount +
                ", price=" + price +
                '}';
    }
}
