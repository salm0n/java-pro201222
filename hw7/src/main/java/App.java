import java.util.Scanner;


public class App {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        DBManager dbManager = new DBManager();

        try{
            try {
                dbManager.init();

                while (flag) {

                    System.out.println("1 - add to database");
                    System.out.println("2 - change in database");
                    System.out.println("3 - view");
                    System.out.println("4 - make an order");
                    System.out.print("-> ");
                    String res = sc.nextLine();

                    switch (res) {
                        case "1" -> dbManager.addToDB();
                        case "2" -> dbManager.changeInDB();
                        case "3" -> dbManager.view();
                        case "4" -> dbManager.makeAnOrder();
                        default -> flag = false;
                    }
                }
            } finally {
                dbManager.close();
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
