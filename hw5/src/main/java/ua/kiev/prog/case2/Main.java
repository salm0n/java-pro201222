package ua.kiev.prog.case2;

import ua.kiev.prog.shared.Client;
import ua.kiev.prog.shared.ConnectionFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException {
        try (Connection conn = ConnectionFactory.getConnection()) {
            try (Statement st = conn.createStatement()) {
                st.execute("DROP TABLE IF EXISTS Clients");
                //st.execute("CREATE TABLE Clients (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(20) NOT NULL, age INT)");
            }

            ClientDAOImpl2 dao = new ClientDAOImpl2(conn, "Clients");
            dao.createTable(Client.class);

            Client c = new Client("test", 1);

            for (int i = 1; i <= 10; i++) {
                dao.add(new Client("user" + i, 30 + i));
            }

            dao.add(c);
            int id = c.getId(); // dz1
            System.out.println(id);

//            List<Client> list = dao.getAll(Client.class);
//            for (Client cli : list)
//                System.out.println(cli);
//
//            list.get(0).setAge(55);
//            dao.update(list.get(0));


            List<Client> list = dao.getAll(Client.class, "name", "age");
//            List<Client> list = dao.getAll(Client.class, "age");
            for (Client cli : list)
                System.out.println(cli);


            dao.delete(list.get(0));
        }
    }
}