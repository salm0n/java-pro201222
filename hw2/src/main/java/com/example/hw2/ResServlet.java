package com.example.hw2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;

@WebServlet(name="Answer", urlPatterns = "/answer")
public class ResServlet extends HttpServlet{

    private static final List<Map<String, String>> answerList = new ArrayList<>();
    private final Map<String, String> li = new HashMap<>();

    public static List<Map<String, String>> getAnswerList() {
        return answerList;
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String javaAnswer = req.getParameter("select_btn1");
        String jsAnswer = req.getParameter("select_btn2");
        String scratchAnswer = req.getParameter("select_btn3");
        String userName = req.getParameter("user__name");

        if(javaAnswer != null && jsAnswer != null && scratchAnswer != null && userName != null && !userName.equals("")) {
            HttpSession session = req.getSession(true);
            Integer idNumber = (Integer) session.getAttribute("id");

            if (idNumber == null){
                idNumber = 1;
                session.setAttribute("id", idNumber);
                li.put("id_number" + idNumber, idNumber.toString());
            }else {
                idNumber++;
                session.setAttribute("id", idNumber);
                li.put("id_number" + idNumber, idNumber.toString());
            }

            session.setAttribute("java_answer_complete", javaAnswer);
            session.setAttribute("js_answer_complete", jsAnswer);
            session.setAttribute("scratch_answer_complete", scratchAnswer);
            session.setAttribute("answer_complete", userName);

            li.put("user_name" + idNumber, userName);
            li.put("java" + idNumber, javaAnswer);
            li.put("js" + idNumber, jsAnswer);
            li.put("scratch" + idNumber, scratchAnswer);

            answerList.add(li);
        }

        resp.sendRedirect("index.jsp");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        String action = req.getParameter("status");
        HttpSession session = req.getSession(true);
        if ("update_question".equals(action) && session != null) {
            session.removeAttribute("answer_complete");
        }else if("reset_question".equals(action) && session != null){
            session.removeAttribute("answer_complete");
            answerList.clear();
            session.setAttribute("id", 0);
            req.getSession(false);
        }
        resp.sendRedirect("index.jsp");
    }


}
