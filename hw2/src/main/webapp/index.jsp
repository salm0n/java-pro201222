<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.example.hw2.ResServlet" %>

<!DOCTYPE html>
<html>
<head>
    <style><%@include file="./style/style.css"%></style>
    <title>Form</title>
</head>
<body>
    <% String answer = (String)session.getAttribute("answer_complete"); %>

    <% if (answer == null || "".equals(answer)){ %>
        <div class="container">
            <form action="/answer" method="post">

                <input type="text" name="user__name" placeholder="Name">

                <p>Do you love Java?</p>

                <label>
                    <input name="select_btn1" type="radio" value="yes">
                    Yes
                </label>
                <label>
                    <input name="select_btn1" type="radio" value="no">
                    No
                </label>

                <p>Do you love JS?</p>

                <label>
                    <input name="select_btn2" type="radio" value="yes">
                    Yes
                </label>
                <label>
                    <input name="select_btn2" type="radio" value="no">
                    No
                </label>

                <p>Do you love Scratch?</p>

                <label>
                    <input name="select_btn3" type="radio" value="yes">
                    Yes
                </label>
                <label>
                    <input name="select_btn3" type="radio" value="no">
                    No
                </label>

                <input type="submit" id="submit__btn">
            </form>
        </div>

    <% }else{ %>
        <div class="container">
            <table class="res_table">
                <tr>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Java</th>
                    <th>Js</th>
                    <th>Scratch</th>
                </tr>
                <% for (int i = 0; i < ResServlet.getAnswerList().size(); i++) {%>
                <% int num = i + 1; %>
                    <tr>
                        <td>
                            <%= ResServlet.getAnswerList().get(i).get("id_number" + num) %>
                        </td>
                        <td>
                            <%= ResServlet.getAnswerList().get(i).get("user_name" + num) %>
                        </td>
                        <td>
                            <%= ResServlet.getAnswerList().get(i).get("java" + num) %>
                        </td>
                        <td>
                            <%= ResServlet.getAnswerList().get(i).get("js" + num) %>
                        </td>
                        <td>
                            <%= ResServlet.getAnswerList().get(i).get("scratch" + num) %>
                        </td>
                    </tr>
                <%
                } %>
            </table>
            <a href="/answer?status=update_question" id="update__btn">update result</a>
            <a href="/answer?status=reset_question" id="reset__btn">reset</a>
        </div>
    <% } %>
</body>
</html>