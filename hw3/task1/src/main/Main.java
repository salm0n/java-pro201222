package main;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        Class<?> cls = Sum.class;

        Method[] methods = cls.getDeclaredMethods();
        Sum sum = new Sum();

        for (Method method : methods){
            if (method.isAnnotationPresent(MyAnnotation.class)){
                MyAnnotation an = method.getAnnotation(MyAnnotation.class);

                try{
                    Object i = method.invoke(sum, an.a(), an.b());
                    System.out.println(i);
                }catch (IllegalAccessException | InvocationTargetException e){
                    e.printStackTrace();
                }
            }
        }
    }
}

class Sum {
    @MyAnnotation(a = 3, b = 4)
    public int calculateSum (int a, int b){
        return a + b;
    }
}

